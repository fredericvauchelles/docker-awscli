#!/bin/bash

if [ -r "$AWS_ACCESS_KEY_ID_FILE" ]; then
    export AWS_ACCESS_KEY_ID=$(cat "$AWS_ACCESS_KEY_ID_FILE");
fi

if [ -r "$AWS_SECRET_ACCESS_KEY_FILE" ]; then
    export AWS_SECRET_ACCESS_KEY=$(cat "$AWS_SECRET_ACCESS_KEY_FILE");
fi

function showPushFileHelp
{
  cat <<EOF
Usage: push [options]
  -f <filename>     target file                   [mandatory]
  -b <dir>          bucket repository directory   [mandatory]
  -n <name>         base name of pushed files     [default=basefilename]
  -c <int>          queue count                   [default=14]

Pushed file format :
  name=myFileName.tar.gz
   -> myFileName_YYYY-MM-DD-HH-mm-ss.tar.gz

Push a file to a bucket repo :
  
  pushFile -f <filename> -b s3://<bucket>/<dir>

Push a file and keep only last 7 pushed files
  
  pushFile -f <filename> -b s3://<bucket>/<dir> -c 7
EOF
}

function showPullFileHelp
{
  cat <<EOF
Usage: pull [options]
  -b <dir>          bucket repository directory   [mandatory]
  -n <name>         base name of pushed files     [mandatory]
  -o <filename>     output filename               [default=remote filename]
EOF
}

function showPromoteHelp
{
  cat <<EOF
  Copy the last upload in the source directory to the target directory

Usage: promote [options]
  -s <dir>          bucket source repository directory   [mandatory]
  -t <dir>          bucket target repository directory   [mandatory]
EOF
}

function showLimitFolderSizeHelp
{
  cat <<EOF
  Copy the last upload in the source directory to the target directory

Usage: limitSize [options]
  -b <dir>          bucket repository directory   [mandatory]
  -c <count>        max number of entries         [default=14]
EOF
}

# Private
# $1 name
# $2 extension
function _getTargetFilename
{
  local datestring=$(date +%Y-%m-%d-%H-%M-%S);
  echo "$1_${datestring}.$2"
}

# $1 bucketDir
function _getLastFileInBucket
{
  local filenames=($(aws s3 ls $1 | awk '{ print $4 }' | sort -r))
  echo ${filenames[0]}
}

# $1 bucket URL
# $2 queue size
function _limitQueueSize
{
  local filenames=$(aws s3 ls $1 | awk '{ print $4 }' | sort -r)
  local index=0
  for filename in $filenames
  do
    index=$(($index + 1))
    if (( $index > $2 )); then
      aws s3 rm $1$filename
    fi
  done
}
# end private

function pushFile
{
  local name=""
  local filename=""
  local bucketDir=""
  local queueCount=14
  local help=false

  while getopts ":n:f:b:c:d:D:h" opt; do
    case $opt in
      n) name="$OPTARG"; ;;
      f) filename="$OPTARG"; ;;
      b) bucketDir="$OPTARG"; ;;
      c) queueCount="$OPTARG"; ;;
      h) help=true; ;;
      *) ;;
    esac
  done

  [ "$help" == "true" ] && showPushFileHelp && exit 0;

  [ -z "$filename" ] && echo "Missing filename" && showPushFileHelp && exit 1;
  [ -z "$bucketDir" ] && echo "Missing bucket directory" && showPushFileHelp && exit 1;

  if [ -z "$name" ]; then
    name=${filename##*/}
    name=${name%%.*}
  fi

  local ext=${filename#*.}
  
  local targetFileName=$(_getTargetFilename $name $ext)
  aws s3 cp $filename $bucketDir$targetFileName

  _limitQueueSize $bucketDir $queueCount
}

function pullFile
{
  local output=""
  local bucketDir=""
  local help=false

  while getopts "o:b:h" opt; do
    case $opt in
      o) output="$OPTARG"; ;;
      b) bucketDir="$OPTARG"; ;;
      h) help=true; ;;
      *) ;;
    esac
  done

  [ "$help" == "true" ] && showPullFileHelp && exit 0;

  [ -z "$bucketDir" ] && echo "Missing bucket directory" && showPullFileHelp && exit 1;

  local remoteFilename=$(_getLastFileInBucket $bucketDir)
  [ -z "$remoteFilename" ] && echo "invalid bucketDir: $bucketDir or empty" && exit 1

  [ -z "$output" ] && output="$remoteFilename"

  aws s3 cp $bucketDir$remoteFilename $output
}

function showHelp
{
  showPushFileHelp
  showPullFileHelp
  showPromoteHelp
  showLimitFolderSizeHelp
}

function promote
{
  local bucketTargetDir=""
  local bucketSourceDir=""
  local help=false

  while getopts "t:s:h" opt; do
    case $opt in
      t) bucketTargetDir="$OPTARG"; ;;
      s) bucketSourceDir="$OPTARG"; ;;
      h) help=true; ;;
      *) ;;
    esac
  done

  [ "$help" == "true" ] && showPromoteHelp && exit 0;

  [ -z "$bucketSourceDir" ] && echo "Missing bucket source directory" && showPromoteHelp && exit 1;
  [ -z "$bucketTargetDir" ] && echo "Missing bucket target directory" && showPromoteHelp && exit 2;

  local remoteFilename=$(_getLastFileInBucket $bucketSourceDir)
  [ -z "$remoteFilename" ] && echo "invalid bucketDir: $bucketSourceDir or empty" && exit 3

  aws s3 cp $bucketSourceDir$remoteFilename $bucketTargetDir$remoteFilename
}

function limitFolderSize
{
  local bucketDir=""
  local queueCount=14
  local help=false

  while getopts "b:c:h" opt; do
    case $opt in
      b) bucketDir="$OPTARG"; ;;
      c) queueCount="$OPTARG"; ;;
      h) help=true; ;;
      *) ;;
    esac
  done

   [ "$help" == "true" ] && showLimitFolderSizeHelp && exit 0;

   [ -z "$bucketDir" ] && echo "Missing bucket directory" && showLimitFolderSizeHelp && exit 1;

   _limitQueueSize $bucketDir $queueCount
}

case "$1" in
  push)
    shift
    pushFile "$@"
    ;;
  pull)
    shift
    pullFile "$@"
    ;;
  promote)
    shift
    promote "$@"
    ;;
  limitSize)
    shift
    limitFolderSize "$@"
    ;;
  *)
    showHelp
    ;;
esac