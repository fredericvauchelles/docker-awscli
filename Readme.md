= S3 File Rotate =

== Push file ==
docker run --volume=$(pwd):/var/ops --rm -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY -e AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION fredericvauchelles/docker-awscli /s3fileRotate.sh push -f /var/ops/target.tar.gz -b s3://fredericvauchelles.odin/${APP}_backups/daily/ -c 30

== Pull file ==
docker run --volume=$(pwd):/var/ops --rm -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY -e AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION fredericvauchelles/docker-awscli /s3fileRotate.sh pull -o /var/ops/target.tar.gz -b s3://fredericvauchelles.odin/${APP}_backups/daily/